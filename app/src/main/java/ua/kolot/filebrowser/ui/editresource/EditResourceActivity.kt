package ua.kolot.filebrowser.ui.editresource

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment


class EditResourceActivity : AppCompatActivity() {

    private var currentFragment: Fragment? = null

    companion object {
        const val ARGS_ID: String = "id"
        const val EDIT_ID: String = "edit"

        fun newInstance(context: Context, id: Int = -1, edit: Boolean = false): Intent {
            val intent = Intent(context, EditResourceActivity::class.java)
            intent.putExtra(ARGS_ID, id);
            intent.putExtra(EDIT_ID, edit)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(ua.kolot.filebrowser.R.layout.edit_resource_activity)
        if (savedInstanceState == null) {
            showContent(intent)
        }
    }

    private fun showContent(intent: Intent) {
        currentFragment = getCurrentFragment(intent);

        if (currentFragment == null) return;
        supportFragmentManager.beginTransaction()
            .replace(ua.kolot.filebrowser.R.id.container, currentFragment!!)
            .addToBackStack(null)
            .commit()


    }

/*
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.getItemId()) {
            android.R.id.home -> {
                //do something here like
                val backStackEntryCount = supportFragmentManager.backStackEntryCount
                if (backStackEntryCount > 0) {
                    supportFragmentManager.popBackStack()
                }
                return true
            }
        }
        return false
    }

    }*/

    override fun onActivityResult(
        requestCode: Int, resultCode: Int,
        resultData: Intent?
    ) {
        super.onActivityResult(requestCode, resultCode, resultData)
        // The ACTION_OPEN_DOCUMENT intent was sent with the request code
        // READ_REQUEST_CODE. If the request code seen here doesn't match, it's the
        // response to some other intent, and the code below shouldn't run at all.

        if (resultCode == Activity.RESULT_OK) {
            // The document selected by the user won't be returned in the intent.
            // Instead, a URI to that document will be contained in the return intent
            // provided to this method as a parameter.
            // Pull that URI using resultData.getData().
            var uri: Uri? = null
            if (resultData != null) {
                uri = resultData.data
                Log.e("test", "Uri: " + uri!!.toString() + " type " + resultData.type)
                currentFragment?.onActivityResult(requestCode, resultCode, resultData)

            }
        }
    }

    override fun onBackPressed() {

        Log.e("TEST", " count stack = " + supportFragmentManager.backStackEntryCount)
        if (supportFragmentManager.backStackEntryCount == 1) {
            finish()
        } else {
            super.onBackPressed()
        }
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        if (intent != null) {
            showContent(intent)
        }
    }

    private fun getCurrentFragment(intent: Intent): Fragment {
        val id = intent.getIntExtra(ARGS_ID, -1)
        val mustBeEdit = intent.getBooleanExtra(EDIT_ID, false)
        return if (id > -1 && !mustBeEdit) {
            ContentFragment.newInstance(id)
        } else EditResourceFragment.newInstance(id)
    }

}
