package ua.kolot.filebrowser.ui.editresource

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import kotlinx.android.synthetic.main.edit_resource_fragment.*
import ua.kolot.filebrowser.MyBrowserApp
import ua.kolot.filebrowser.data.model.Resource
import ua.kolot.filebrowser.data.model.Type
import ua.kolot.filebrowser.databinding.EditResourceFragmentBinding


class EditResourceFragment : Fragment() {

    companion object {
        fun newInstance(id: Int): EditResourceFragment {
            val fragment = EditResourceFragment()
            val args = Bundle()
            args.putInt("id", id)
            fragment.arguments = args
            return fragment
        }
    }

    private var binding: EditResourceFragmentBinding? = null
    private lateinit var viewModel: EditResourceViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
      //val view = inflater.inflate(ua.kolot.filebrowser.R.layout.edit_resource_fragment, container, false)
        val factory = EditViewModelFactory(
            (activity?.application as MyBrowserApp)
        )
        viewModel = ViewModelProviders.of(this, factory).get(EditResourceViewModel::class.java)
        binding =DataBindingUtil.inflate(inflater,
                ua.kolot.filebrowser.R.layout.edit_resource_fragment,container, false)

        binding?.viewModel = viewModel;
        binding?.resource = Resource(1, "ww", "ww", "www" , Type("none"))
        val passId = arguments?.getInt(("id"), 0) ?: 0
        if (passId > 0) {
            viewModel.getById(passId)?.observe(this, Observer {
                binding?.resource = it
            })

        }

        viewModel.saveSuccessEvent().observe(this, Observer { successEvent ->
            if (successEvent) activity?.onBackPressed()
        })


        return binding?.root?:inflater.inflate(ua.kolot.filebrowser.R.layout.edit_resource_fragment, container, false)
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setHasOptionsMenu(true);
        val activity = activity as AppCompatActivity?
        val toolbar = activity?.findViewById(ua.kolot.filebrowser.R.id.toolbar) as Toolbar

        activity.setSupportActionBar(toolbar)
        activity.supportActionBar?.setDisplayHomeAsUpEnabled(true)
        activity.supportActionBar?.setDefaultDisplayHomeAsUpEnabled(true)
        toolbar.setNavigationOnClickListener {
            activity.onBackPressed()
        }

        choose.setOnClickListener(View.OnClickListener {
            requestFile()
        })

    }

    private fun checkPermission():Boolean{
       /* if (ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE)
            != PackageManager.PERMISSION_GRANTED)*/
        return true
    }


    private fun requestFile() {
        val chooseFile: Intent
        val intent: Intent
        chooseFile = Intent(Intent.ACTION_GET_CONTENT)
        chooseFile.addCategory(Intent.CATEGORY_OPENABLE)
        chooseFile.type = "*/*"
        intent = Intent.createChooser(chooseFile, "Choose a file")
        startActivityForResult(intent, 8858)
    }


    override fun onActivityResult(
        requestCode: Int, resultCode: Int,
        resultData: Intent?
    ) {
        super.onActivityResult(requestCode, resultCode, resultData)
        // The ACTION_OPEN_DOCUMENT intent was sent with the request code
        // READ_REQUEST_CODE. If the request code seen here doesn't match, it's the
        // response to some other intent, and the code below shouldn't run at all.

        if (resultCode == Activity.RESULT_OK) {
            // The document selected by the user won't be returned in the intent.
            // Instead, a URI to that document will be contained in the return intent
            // provided to this method as a parameter.
            // Pull that URI using resultData.getData().
            var uri: String? = null
            if (resultData != null) {
                uri = resultData.data.path
                Log.e("test", "Uri: " + uri!!.toString())
              // path.setText(uri.toString())

                val cR = context!!.contentResolver
                val mime = cR.getType(resultData.data)
                val newResource =  binding?.resource
                newResource?.path = uri.toString()
                newResource?.type = Type(mime)
                binding?.resource = newResource

            }
        }
    }

    class EditViewModelFactory(var myApp: MyBrowserApp) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            val editResourceViewModel: EditResourceViewModel =
                myApp.applicationComponent.editResourceViewModel()
            return editResourceViewModel as T
        }

    }
}
