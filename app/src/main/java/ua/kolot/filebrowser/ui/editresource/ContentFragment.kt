package ua.kolot.filebrowser.ui.editresource

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import kotlinx.android.synthetic.main.content_fragment.*
import ua.kolot.filebrowser.MyBrowserApp
import ua.kolot.filebrowser.R
import ua.kolot.filebrowser.data.model.Resource
import ua.kolot.filebrowser.data.model.Type
import ua.kolot.filebrowser.databinding.ContentFragmentBinding


class ContentFragment : Fragment() {


    companion object {
        fun newInstance(id: Int): ContentFragment {
            val fragment = ContentFragment()
            val args = Bundle()
            args.putInt("id", id)
            fragment.arguments = args
            return fragment
        }
    }

    private lateinit var viewModel: ContentViewModel
    private lateinit var binding:ContentFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val factory = ContentFragment.ContentViewModelFactory(
            (activity?.application as MyBrowserApp)
        )
        viewModel = ViewModelProviders.of(this, factory).get(ContentViewModel::class.java)
        viewModel.currentId = arguments?.getInt("id") ?: -1
        binding = DataBindingUtil.inflate(inflater,
            R.layout.content_fragment,container, false)

        binding.resource = Resource(1, "", "", "", Type("none"))
        viewModel.get().observe(this, Observer {
            binding.resource = it
            binding.toolbar.title = it.name

        })



        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(ContentViewModel::class.java)

        val activity = activity as AppCompatActivity?
        activity?.setSupportActionBar(binding.toolbar)
        activity?.supportActionBar?.setDisplayHomeAsUpEnabled(true)
        toolbar.setNavigationOnClickListener {
            activity?.onBackPressed() }
        editFab.setOnClickListener { startActivity(context?.let { context ->
            EditResourceActivity.newInstance(
                context, viewModel.currentId, true)
        }) }
    }

    class ContentViewModelFactory(var myApp: MyBrowserApp) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            val contentViewModel: ContentViewModel =
                myApp.applicationComponent.contentResourceViewModel()
            return contentViewModel as T
        }

    }
}
