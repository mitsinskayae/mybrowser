package ua.kolot.filebrowser.ui.editresource

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import ua.kolot.filebrowser.data.model.Resource
import ua.kolot.filebrowser.data.source.ResourceRepository
import javax.inject.Inject

class ContentViewModel @Inject constructor(
    public var repository: ResourceRepository

) : ViewModel() {
    public var currentId: Int = -1
        set(value) {
            load()
            field = value
        }
    private var currentResource: MediatorLiveData<Resource> = MediatorLiveData()
    private val viewModelScope: CoroutineScope = CoroutineScope(Dispatchers.Main)

    private fun load() {
        viewModelScope.launch(Dispatchers.Default) {
            val resource = repository.getById(currentId)
            currentResource.addSource(resource, Observer {
                currentResource.postValue(it)
            })
        }
    }


    fun get(): LiveData<Resource> {
        return currentResource
    }
}
